/*
* Vanitygen, vanity bitcoin address generator
* Copyright (C) 2011 <samr7@cs.washington.edu>
*
* Vanitygen is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* Vanitygen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with Vanitygen.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <pthread.h>

#include <openssl/sha.h>
#include <openssl/ripemd.h>
#include <openssl/ec.h>
#include <openssl/bn.h>
#include <openssl/rand.h>

#include "pattern.h"
#include "util.h"

const char *version = VANITYGEN_VERSION;


/*
* Address search thread main loop
*/

void *
vg_thread_loop(void *arg)
{
	unsigned char hash_buf[128];
	unsigned char *eckey_buf;
	unsigned char hash1[32];

	int i, c, len, output_interval;
	int hash_len;

	const BN_ULONG rekey_max = 10000000;
	BN_ULONG npoints, rekey_at, nbatch;

	vg_context_t *vcp = (vg_context_t *)arg;
	EC_KEY *pkey = NULL;
	const EC_GROUP *pgroup;
	const EC_POINT *pgen;
	const int ptarraysize = 256;
	EC_POINT *ppnt[256];
	EC_POINT *pbatchinc;

	vg_partial_match_func_t partial_math_func = vcp->vc_partial_match_func;
	vg_test_func_t test_func = vcp->vc_test;
	vg_exec_context_t ctx;
	vg_exec_context_t *vxcp;

	struct timeval tvstart;


	memset(&ctx, 0, sizeof(ctx));
	vxcp = &ctx;

	vg_exec_context_init(vcp, &ctx);

	pkey = vxcp->vxc_key;
	pgroup = EC_KEY_get0_group(pkey);
	pgen = EC_GROUP_get0_generator(pgroup);

	for (i = 0; i < ptarraysize; i++) {
		ppnt[i] = EC_POINT_new(pgroup);
		if (!ppnt[i]) {
			fprintf(stderr, "ERROR: out of memory?\n");
			exit(1);
		}
	}
	pbatchinc = EC_POINT_new(pgroup);
	if (!pbatchinc) {
		fprintf(stderr, "ERROR: out of memory?\n");
		exit(1);
	}

	BN_set_word(&vxcp->vxc_bntmp, ptarraysize);
	EC_POINT_mul(pgroup, pbatchinc, &vxcp->vxc_bntmp, NULL, NULL,
		vxcp->vxc_bnctx);
	EC_POINT_make_affine(pgroup, pbatchinc, vxcp->vxc_bnctx);

	npoints = 0;
	rekey_at = 0;
	nbatch = 0;
	vxcp->vxc_key = pkey;
	vxcp->vxc_binres[0] = vcp->vc_addrtype;
	c = 0;
	output_interval = 1000;
	gettimeofday(&tvstart, NULL);

	if (vcp->vc_format == VCF_SCRIPT) {
		hash_buf[0] = 0x51;  // OP_1
		hash_buf[1] = 0x41;  // pubkey length
							 // gap for pubkey
		hash_buf[67] = 0x51;  // OP_1
		hash_buf[68] = 0xae;  // OP_CHECKMULTISIG
		eckey_buf = hash_buf + 2;
		hash_len = 69;

	}
	else {
		eckey_buf = hash_buf;
		hash_len = 65;
	}

	while (!vcp->vc_halt) {
		if (++npoints >= rekey_at) {
			vg_exec_context_upgrade_lock(vxcp);
			/* Generate a new random private key */
			EC_KEY_generate_key(pkey);
			npoints = 0;

			/* Determine rekey interval */
			EC_GROUP_get_order(pgroup, &vxcp->vxc_bntmp,
				vxcp->vxc_bnctx);
			BN_sub(&vxcp->vxc_bntmp2,
				&vxcp->vxc_bntmp,
				EC_KEY_get0_private_key(pkey));
			rekey_at = BN_get_word(&vxcp->vxc_bntmp2);
			if ((rekey_at == BN_MASK2) || (rekey_at > rekey_max))
				rekey_at = rekey_max;
			assert(rekey_at > 0);

			EC_POINT_copy(ppnt[0], EC_KEY_get0_public_key(pkey));
			vg_exec_context_downgrade_lock(vxcp);

			npoints++;
			vxcp->vxc_delta = 0;

			if (vcp->vc_pubkey_base)
				EC_POINT_add(pgroup,
					ppnt[0],
					ppnt[0],
					vcp->vc_pubkey_base,
					vxcp->vxc_bnctx);

			for (nbatch = 1;
				(nbatch < ptarraysize) && (npoints < rekey_at);
				nbatch++, npoints++) {
				EC_POINT_add(pgroup,
					ppnt[nbatch],
					ppnt[nbatch - 1],
					pgen, vxcp->vxc_bnctx);
			}

		}
		else {
			/*
			* Common case
			*
			* EC_POINT_add() can skip a few multiplies if
			* one or both inputs are affine (Z_is_one).
			* This is the case for every point in ppnt, as
			* well as pbatchinc.
			*/
			assert(nbatch == ptarraysize);
			for (nbatch = 0;
				(nbatch < ptarraysize) && (npoints < rekey_at);
				nbatch++, npoints++) {
				EC_POINT_add(pgroup,
					ppnt[nbatch],
					ppnt[nbatch],
					pbatchinc,
					vxcp->vxc_bnctx);
			}
		}

		/*
		* The single most expensive operation performed in this
		* loop is modular inversion of ppnt->Z.  There is an
		* algorithm implemented in OpenSSL to do batched inversion
		* that only does one actual BN_mod_inverse(), and saves
		* a _lot_ of time.
		*
		* To take advantage of this, we batch up a few points,
		* and feed them to EC_POINTs_make_affine() below.
		*/

		EC_POINTs_make_affine(pgroup, nbatch, ppnt, vxcp->vxc_bnctx);

		for (i = 0; i < nbatch; i++, vxcp->vxc_delta++) {
			/* Hash the public key */
			len = EC_POINT_point2oct(pgroup, ppnt[i],
				POINT_CONVERSION_UNCOMPRESSED,
				eckey_buf,
				65,
				vxcp->vxc_bnctx);
			assert(len == 65);

			SHA256(hash_buf, hash_len, hash1);
			RIPEMD160(hash1, sizeof(hash1), &vxcp->vxc_binres[1]);

			if(vxcp->vxc_vc->vc_partial_match)
				partial_math_func(vxcp, ppnt[i], pkey);

			switch (test_func(vxcp)) {
			case 1:
				npoints = 0;
				rekey_at = 0;
				i = nbatch;
				break;
			case 2:
				goto out;
			default:
				break;
			}
			//MARK: here we have adress and privkey in each thread
			//vg_exec_context_upgrade_lock(vxcp);
		
			//vg_exec_context_downgrade_lock(vxcp);
		/*	char addr_buf[64], addr2_buf[64];
			char privkey_buf[VG_PROTKEY_MAX_B58];
			const char *keytype = "Privkey";
			vg_encode_address(ppnt[i],
				EC_KEY_get0_group(pkey),
				vcp->vc_pubkeytype, addr_buf);
			vg_encode_privkey(pkey, vcp->vc_privtype, privkey_buf);

			printf("Address: %s\n"
				"%s: %s\n",
				addr_buf, keytype, privkey_buf);*/
		}

		c += i;
		if (c >= output_interval) {
			output_interval = vg_output_timing(vcp, c, &tvstart);
			if (output_interval > 250000)
				output_interval = 250000;
			c = 0;
		}

		vg_exec_context_yield(vxcp);
	}

out:
	vg_exec_context_del(&ctx);
	vg_context_thread_exit(vcp);

	for (i = 0; i < ptarraysize; i++)
		if (ppnt[i])
			EC_POINT_free(ppnt[i]);
	if (pbatchinc)
		EC_POINT_free(pbatchinc);
	return NULL;
}


#if !defined(_WIN32)
int
count_processors(void)
{
	FILE *fp;
	char buf[512];
	int count = 0;

	fp = fopen("/proc/cpuinfo", "r");
	if (!fp)
		return -1;

	while (fgets(buf, sizeof(buf), fp)) {
		if (!strncmp(buf, "processor\t", 10))
			count += 1;
	}
	fclose(fp);
	return count;
}
#endif

int
start_threads(vg_context_t *vcp, int nthreads)
{
	pthread_t thread;

	if (nthreads <= 0) {
		/* Determine the number of threads */
		nthreads = count_processors();
		if (nthreads <= 0) {
			fprintf(stderr,
				"ERROR: could not determine processor count\n");
			nthreads = 1;
		}
	}

	if (vcp->vc_verbose > 1) {
		fprintf(stderr, "Using %d worker thread(s)\n", nthreads);
	}

	while (--nthreads) {
		if (pthread_create(&thread, NULL, vg_thread_loop, vcp))
			return 0;
	}

	vg_thread_loop(vcp);
	return 1;
}

void
usage(const char *name)
{
	fprintf(stderr,
		"Justgen %s (" OPENSSL_VERSION_TEXT ")\n"
		"Usage: %s [-vqnrik1NT] [-t <threads>] [-f <filename>|-] [<pattern>...]\n"
		"Generates a bitcoin receiving address matching <pattern>, and outputs the\n"
		"address and associated private key.  The private key may be stored in a safe\n"
		"location or imported into a bitcoin client to spend any balance received on\n"
		"the address.\n"
		"By default, <pattern> is interpreted as an exact prefix.\n"
		"\n"
		"Options:\n"
		"-k            Keep pattern and continue search after finding a match\n"
		"-f <file>     File containing list of patterns, one per line\n"
		"              (Use \"-\" as the file name for stdin)\n"
		"-o <file>     Write pattern matches to <file>\n"
		"-p <value>    Enable partial matching. <value> 0..1 set matching sensity\n"
		"-l <value>    Enable logging. <value> is logging period on seconds\n"
		"Note: max addr len wouldn't more then 33 symbols \n",
		version, name);
}

#define MAX_FILE 4

int
main(int argc, char **argv)
{
	int addrtype = 0;
	int privtype = 128;
	double pm_value = 0;
	long long pm_found = 0ll;
	vg_partial_match_t *vpm = NULL;
	unsigned int log_period = 0;
	time_t log_lastcall = 0ll;
	time_t log_start_time = time(NULL);
	char log_filepath[30];
	vg_log *vlg = NULL;
	int pubkeytype;
	enum vg_format format = VCF_PUBKEY;
	int caseinsensitive = 0;
	int verbose = 1;
	int remove_on_match = 1;
	int only_one = 0;
	int opt;
	const char *result_file = NULL;
	const char *key_password = NULL;
	const char *pm_result_file = "partial_result.txt";
	char **patterns;
	int npatterns = 0;
	int nthreads = 0;
	vg_context_t *vcp = NULL;
	EC_POINT *pubkey_base = NULL;

	FILE *pattfp[MAX_FILE], *fp;
	int pattfpi[MAX_FILE];
	int npattfp = 0;
	int pattstdin = 0;

	int i;
	while ((opt = getopt(argc, argv, "vqnrik1eE:P:NTX:F:t:h?f:o:s:p:l:")) != -1) {
		switch (opt) {
		case 'k':
			remove_on_match = 0;
			break;
		case 'f':
			if (npattfp >= MAX_FILE) {
				fprintf(stderr,
					"Too many input files specified\n");
				return 1;
			}
			if (!strcmp(optarg, "-")) {
				if (pattstdin) {
					fprintf(stderr, "ERROR: stdin "
						"specified multiple times\n");
					return 1;
				}
				fp = stdin;
			}
			else {
				fp = fopen(optarg, "r");
				if (!fp) {
					fprintf(stderr,
						"Could not open %s: %s\n",
						optarg, strerror(errno));
					return 1;
				}
			}
			pattfp[npattfp] = fp;
			pattfpi[npattfp] = caseinsensitive;
			npattfp++;
			break;
		case 'o':
			if (result_file) {
				fprintf(stderr,
					"Multiple output files specified\n");
				return 1;
			}
			result_file = optarg;
			break;
		case 'p':
			pm_value = atof(optarg);
			if (pm_value < 0 || pm_value>1) {
				fprintf(stderr,
					"Incorrect partial sensity koeff %s\n", optarg);
				return 1;
			}
			vpm = (vg_partial_match_t*)malloc(sizeof(vg_partial_match_t));
			vpm->vc_filepath = pm_result_file;
			vpm->vc_pm_value = pm_value;
			vpm->vc_npatterns = npatterns;
			vpm->vc_pm_found = pm_found;
			vpm->vc_patterns = (const char**)malloc(sizeof(const char**));
			break;
		case 'l':
			log_period = atoi(optarg);
			vlg = (vg_log*)malloc(sizeof(vg_log));
			strcpy(log_filepath, ctime(&log_start_time));
			strrep(log_filepath, ':', '_');
			strrep(log_filepath, ' ', '-');
			strrep(log_filepath, '\n', '.');
			strcat(log_filepath, "log");
			vlg->vc_filepath = log_filepath;
			vlg->vc_lastcall = log_lastcall;
			vlg->vc_period = log_period;
			break;
		default:
			usage(argv[0]);
			return 1;
		}
	}

#if OPENSSL_VERSION_NUMBER < 0x10000000L
	/* Complain about older versions of OpenSSL */
	if (verbose > 0) {
		fprintf(stderr,
			"WARNING: Built with " OPENSSL_VERSION_TEXT "\n"
			"WARNING: Use OpenSSL 1.0.0d+ for best performance\n");
	}
#endif
	pubkeytype = addrtype;


	vcp = vg_prefix_context_new(addrtype, privtype, caseinsensitive);

	vcp->vc_verbose = verbose;
	vcp->vc_result_file = result_file;
	vcp->vc_remove_on_match = remove_on_match;
	vcp->vc_only_one = only_one;
	vcp->vc_format = format;
	vcp->vc_pubkeytype = pubkeytype;
	vcp->vc_pubkey_base = pubkey_base;

	vcp->vc_partial_match = vpm;
	vcp->vc_partial_match_func = vg_partial_match;

	vcp->vc_output_match = vg_output_match_console;
	vcp->vc_output_timing = vg_output_timing_console;

	vcp->vc_logging = vlg;
	vcp->vc_output_log = vg_output_log_file;

	if (!npattfp) {
		if (optind >= argc) {
			usage(argv[0]);
			return 1;
		}
		patterns = &argv[optind];
		npatterns = argc - optind;

		for (int i = 0; i < npatterns; i++)
			if (strlen(patterns[i]) > MAX_ADDR_LEN)
				sprintf(patterns[i], "%.*s", MAX_ADDR_LEN, patterns[i]);

		if (!vg_context_add_patterns(vcp,
			(const char ** const)patterns,
			npatterns))
			return 1;
	}

	for (i = 0; i < npattfp; i++) {
		fp = pattfp[i];
		if (!vg_read_file(fp, &patterns, &npatterns)) {
			fprintf(stderr, "Failed to load pattern file\n");
			return 1;
		}
		if (fp != stdin)
			fclose(fp);

		vg_prefix_context_set_case_insensitive(vcp, pattfpi[i]);

		for (int i = 0; i < npatterns; i++)
			if (strlen(patterns[i]) > MAX_ADDR_LEN)
				sprintf(patterns[i], "%.*s", MAX_ADDR_LEN, patterns[i]);

		if (!vg_context_add_patterns(vcp,
			(const char ** const)patterns,
			npatterns))
			return 1;
	}
	
	if (!vcp->vc_npatterns) {
		fprintf(stderr, "No patterns to search\n");
		return 1;
	}

	vcp->vc_key_protect_pass = key_password;

	if (!start_threads(vcp, nthreads))
		return 1;
	return 0;
}
