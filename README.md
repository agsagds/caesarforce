# CaesarForce
Сброка под Linux
	make justgen
Сброка под Windows
	см. Deploy.docx (vanitygen везде заменять на justgen)

Запуск 
Linux: ./justgen -f patterns.txt -o results.txt -k
Windows: 	justgen.exe -f patterns.txt -o results.txt -k
	-f <путь к файлу с шаблонами>
	-o <путь к выходному файлу>
	-k продолжать поиск после первого совпадения
	
Длина шаблона должна быть на 3 меньше максимальной длины ключа, т.к. последние 3 символа являются контрольной суммой и всегда однозначно определяются ведущей частью
